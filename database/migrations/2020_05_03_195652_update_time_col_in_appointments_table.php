<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTimeColInAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            // $table->time('time')->nullable()->change();
            // $table->dateTimeTz('appointment_date_time')->nullable();    
            $table->string('start_url')->nullable();    
            $table->string('join_url')->nullable();    
            $table->string('meeting_uuid')->nullable();    
            $table->string('meeting_password')->nullable();    
            $table->string('meeting_id')->nullable();    
            $table->string('meeting_host_id')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            //
        });
    }
}
