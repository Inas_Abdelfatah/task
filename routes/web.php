<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('admin.index');
// });

Route::get('/user', 'UserController@index');

Route::namespace('Admin')->prefix('dashboard')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::get('/login', 'AuthController@login')->name('admin.login');
    Route::post('/login', 'AuthController@postLogin')->name('admin.postLogin');
    Route::post('/logout', 'AuthController@logout')->name('admin.logout');
    Route::get('/', 'HomeController@index')->name('admin.dashboard');
    Route::get('/hosts', 'HostController@index')->name('admin.hosts');
    Route::post('/host', 'HostController@store')->name('admin.add_host');
    Route::get('/appointments/{host_id}', 'AppointmentController@index')->name('admin.host_appointments');
    Route::post('/appointment', 'AppointmentController@store')->name('admin.add_appointment');
});

Route::namespace('Frontend')->group(function () {
    Route::get('/login', 'AuthController@login')->name('frontend.login');
    Route::post('/login', 'AuthController@postLogin')->name('frontend.postLogin');
    Route::get('/register', 'UserController@register')->name('frontend.register');
    Route::post('/register', 'UserController@store')->name('frontend.postRegister');
    Route::post('/logout', 'AuthController@logout')->name('frontend.logout');
    Route::get('/', 'HomeController@index')->name('frontend.dashboard');
    Route::get('/attendee-appointments/{attendee_id}', 'AppointmentController@getAttendeeAppointments')->name('frontend.attendee_appointments');
    Route::get('/host-appointments/{host_id}', 'AppointmentController@getHostAppointments')->name('frontend.host_appointments');
    Route::get('/reserve-appointment/{id}', 'AppointmentController@reserve')->name('frontend.reserve_appointment');
    Route::get('/appointment/join-meeting/{appointment_id}', 'AppointmentController@joinMeeting')->name('frontend.joinMeeting');
});
