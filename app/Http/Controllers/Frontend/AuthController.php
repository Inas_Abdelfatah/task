<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::user())
            return redirct()->route('frontend.dashboard');
        return view('frontend.auth.login');
    }

    public function postLogin(Request $request)
    { 
        $credentials = $request->only('email', 'password');
        $credentials_validator = [
            'email' => 'required|email',
            'password' => 'required'
        ];
        $validator = Validator::make($credentials, $credentials_validator);
        if ($validator->fails()) {
            return back()->with('errors',$validator->errors());
        }

        //$credentials['role'] = 3;
        if (Auth::attempt($credentials))
            return redirect()->route('frontend.dashboard');
        else
            return back()->with('error','Email or password is wrong');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('frontend.login');

    }
    
}
