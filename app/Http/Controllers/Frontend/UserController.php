<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{

    private $userRepo;
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function register()
    {
        if(Auth::user())
            return redirct()->route('frontend.dashboard');
        return view('frontend.auth.register');
    }

    public function store(StoreUserRequest $request){
    	$data = $request->only('first_name', 'last_name', 'email', 'password', 'role');
    	$user = $this->userRepo->save($data);
    	if($user)
    		return redirect()->route('frontend.login')->with('success','your account created successfully. login in to the system');
    	return redirect()->route('frontend.register')->with('error','something went wrong, please try again');
    }
}
