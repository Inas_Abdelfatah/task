<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AppointmentRepository;

class HomeController extends Controller
{
	private $appointmentRepo;
    public function __construct( AppointmentRepository $appointmentRepo)
    {
        $this->appointmentRepo = $appointmentRepo;
        $this->middleware('auth');
    }

    public function index()
    {   
        if(auth()->user()->role == 2):
            return redirect()->route('frontend.host_appointments',['host_id' => auth()->user()->id]);
        else:
            $appointments = $this->appointmentRepo->getNonReservedAppointments();
            return view('frontend.index',compact('appointments'));
        endif;
    }
}
