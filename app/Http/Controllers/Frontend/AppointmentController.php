<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AppointmentRepository;
use App\Appointment;

class AppointmentController extends Controller
{

    private $appointmentRepo;

    public function __construct(AppointmentRepository $appointmentRepo)
    {
        $this->middleware('auth');
        $this->appointmentRepo = $appointmentRepo;
    }

    public function index()
    {   
        $appointments = $this->appointmentRepo->getNonReservedAppointments();
        return view('frontend.index',compact('appointments'));
    }

    public function store(StoreAppointmentRequest $request){
    	$data = $request->only('title','host_id', 'attendee_id', 'date', 'time');
    	$appointment = $this->appointmentRepo->save($data);
    	if($appointment)
    		return redirect()->route('admin.host_appointments',['host_id' => $data['host_id']])->with('success','appointment added successfully');
    	return redirect()->route('admin.host_appointments',['host_id' => $data['host_id']])->with('error','something wrong, please try again');

    }

    public function reserve($id){
        $appointment = $this->appointmentRepo->reserveAppointment($id);
        if($appointment)
            return back()->with('success','appointment reserved successfully');
        return back()->with('error','something went wrong, please try again');

    }

    public function getAttendeeAppointments($attendee_id)
    {   
        $appointments = $this->appointmentRepo->getAttendeeAppointments($attendee_id);
        return view('frontend.appointments',compact('appointments'));
    }

    public function joinMeeting($appointment_id){
        $appointment = Appointment::find($appointment_id);
        return view('frontend.appointment',compact('appointment'));
    }

    public function getHostAppointments($host_id)
    {   
        $appointments = $this->appointmentRepo->getHostAppointments($host_id,10);
        return view('frontend.host_appointments',compact('appointments'));
    }
}
