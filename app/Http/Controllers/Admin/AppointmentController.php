<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AppointmentRepository;
use App\Http\Requests\StoreAppointmentRequest;
use App\Repositories\UserRepository;

class AppointmentController extends Controller
{

    private $appointmentRepo;

    public function __construct(AppointmentRepository $appointmentRepo)
    {
        $this->middleware('auth');
        $this->appointmentRepo = $appointmentRepo;
    }

    public function index($host_id)
    {   
        $appointments = $this->appointmentRepo->getHostAppointments($host_id,10);
        return view('admin.appointments',compact('appointments', 'host_id'));
    }

    public function store(StoreAppointmentRequest $request){
    	$data = $request->only('title','host_id', 'attendee_id', 'date', 'time', 'end_time');
    	$appointment = $this->appointmentRepo->save($data);
    	if($appointment)
    		return redirect()->route('admin.host_appointments',['host_id' => $data['host_id']])->with('success','appointment added successfully');
    	return redirect()->route('admin.host_appointments',['host_id' => $data['host_id']])->with('error','something wrong, please try again');

    }
}
