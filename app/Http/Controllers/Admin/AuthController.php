<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::user())
            return redirct()->route('admin.dashboard');
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    { 
        $credentials = $request->only('email', 'password');
        $credentials_validator = [
            'email' => 'required|email',
            'password' => 'required'
        ];
        $validator = Validator::make($credentials, $credentials_validator);
        if ($validator->fails()) {
            return back()->with('errors',$validator->errors());
        }

        $credentials['role'] = 1;
        if (Auth::attempt($credentials))
            return redirect()->route('admin.dashboard');
        else
            return back()->with('error','Email or password is wrong');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('admin.login');

    }
    
}
