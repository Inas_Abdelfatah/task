<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use App\Http\Requests\StoreUserRequest;

class HostController extends Controller
{

    private $userRepo;
    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth');
        $this->userRepo = $userRepo;
    }

    public function index()
    {   
        if(Auth::user()->role == 1){

            $hosts = $this->userRepo->getHosts(15);
            return view('admin.hosts',compact('hosts'));
        }
        return redirect()->route('admin.dashboard')->with('error','you don\'t have permission to access this page');
    }

    public function store(StoreUserRequest $request){
    	$data = $request->only('first_name', 'last_name', 'email', 'password', 'role');
    	$host = $this->userRepo->save($data);
    	if($host)
    		return redirect()->route('admin.hosts')->with('success','host added successfully');
    	return redirect()->route('admin.hosts')->with('error','something wrong, please try again');

    }
}
