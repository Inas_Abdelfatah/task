<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    //'title','host_id', 'attendee_id', 'date', 'time'date('Y-m-d H:i:s');
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:255', 'min:1'],
            'host_id' => ['required', 'integer', 'min:1'],
            'attendee_id' => ['nullable', 'integer', 'min:1'],
            'date' => ['required', 'date', 'date_format:Y-m-d', 'after_or_equal:'.date('Y-m-d')],
            'time' => ['required', 'date_format:H:i'],
            'end_time' => ['required', 'date_format:H:i', 'after_or_equal:'.$this->time],
            //'time' => ['required', 'date_format:H:i', 'after_or_equal:'.date('H:i')],
        ];
    }
}
