<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Appointment extends Model
{

    protected $fillable = ['title','host_id', 'attendee_id', 'date', 'time', 'start_url', 'join_url', 'meeting_uuid', 'meeting_password', 'meeting_id', 'meeting_host_id', 'end_time', 'duration'];

    function host()
    {
        return $this->belongsTo(User::class,'host_id');
    }

    function attendee()
    {
        return $this->belongsTo(User::class,'attendee_id');
    }
}
