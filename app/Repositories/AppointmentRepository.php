<?php

namespace App\Repositories;

use App\Appointment;
use Illuminate\Http\Request;

class AppointmentRepository
{
    /**
     * Get all appointments
     * $perPage
     *
     * @param int $host_id, $perPage
     * @return array
     */

    public function getHostAppointments($host_id, $perPage = 10)
    {
        $data = [];
        $data['reserved_appointments'] = Appointment::whereHostId($host_id)->whereNotNull('attendee_id')->latest()->paginate($perPage);
        $data['reserved_appointments']->map(function ($q) {
            if($q->date == date('Y-m-d')){
                $diff = strtotime($q->end_time) - strtotime(date('H:i'));
                if($diff <= 0){
                    $q->is_valid = 0;
                }else{
                    $q->is_valid = 1;
                }
            }elseif($q->date > date('Y-m-d')){
                $q->is_valid = 1;
            }else{
                $q->is_valid = 0;
            }
            return $q;
        }); 
        $data['non_reserved_appointments'] = Appointment::whereHostId($host_id)->whereNull('attendee_id')->latest()->paginate($perPage);

        return $data;
    }

    /**
     * Create new Appointment.
     *
     * @param $data
     * @return Appointment
     **/
    public function save($data)
    {
        $data['duration'] = round(abs(strtotime($data['end_time'] ) - strtotime($data['time'] )) / 60,2);
         $meeting_data = $this->create_meeting($data);
        $meeting = [
            'start_url' => $meeting_data->start_url,
            'join_url' => $meeting_data->join_url,
            'meeting_uuid' => $meeting_data->uuid,
            'meeting_id' => $meeting_data->id,
            'meeting_password' => $meeting_data->password,
            'meeting_host_id' => $meeting_data->host_id,
            'duration' => $meeting_data->duration,

        ];
        $data = array_merge($data, $meeting);
        $appointment = Appointment::create($data);
        return $appointment;
    }

    public function getNonReservedAppointments(){
        $appointments = Appointment::whereNull('attendee_id')->where('date','>=',date('Y-m-d'))->get();
        // $appointments->reject(function ($q) {
        //     $q->when($q->date == date('Y-m-d'), function ($q) { 
        //         return $q->whereNotNull('end_time')->where('end_time','<=',date('H:i'));
        //     });
                
        // });

        $appointments->filter(function ($q) {
            if($q->date == date('Y-m-d')){
                $diff = strtotime($q->end_time) - strtotime(date('H:i'));
                if($diff > 0){
                    return $q;
                }
            }else{
                return $q;
            }
        })->all();
        // $appointments->reject(function ($q) {
        //     return $q->diff <= 0 ;
        // }); 
        //dd($appointments);
        return $appointments;
    }

    public function reserveAppointment($id)
    {
        $appointment = Appointment::find($id);
        if($appointment){
            $appointment->update(['attendee_id' => auth()->user()->id]);
            return true;
        }
        return false;
    }

    public function getAttendeeAppointments($attendee_id)
    {   
        $appointments = Appointment::whereAttendeeId($attendee_id)->get();
        $appointments->map(function ($q) {
            if($q->date == date('Y-m-d')){
                $diff = strtotime($q->end_time) - strtotime(date('H:i'));
                if($diff <= 0){
                    $q->is_valid = 0;
                }else{
                    $q->is_valid = 1;
                }
            }elseif($q->date > date('Y-m-d')){
                $q->is_valid = 1;
            }else{
                $q->is_valid = 0;
            }
            return $q;
        }); 
        return $appointments;
    }


function create_meeting($appointment) {
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.zoom.us']);
    try {
        $response = $client->request('POST', '/v2/users/me/meetings', [
            "headers" => [
                "Authorization" => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IjBlOFIwYl93UVFHOHh6YTB1ZkQ4OVEiLCJleHAiOjE1ODkxMzkwMzQsImlhdCI6MTU4ODUzNDIzNn0.b90PGygKyW5COecNyFlydrF7uV1HpGI6756i9iNMcc8"
            ],
            'json' => [
                "topic" => $appointment['title'],
                "type" => 2,
                "start_time" => $appointment['date'].$appointment['time'],
                "duration" => $appointment['duration'], // 30 mins
                "password" => "123456"
            ],
        ]);
 
        $data = json_decode($response->getBody());
        return $data;
 
    } catch(Exception $e) {
        if( 401 == $e->getCode() ) {
            return 'token has been expired';
        } else {
            return $e->getMessage();
        }
    }
}
 
}
