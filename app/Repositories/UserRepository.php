<?php

namespace App\Repositories;

use App\User;
use App\Mail\UserMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserRepository
{
    /**
     * Get all hosts
     * $perPage
     *
     * @param int $perPage
     * @return array
     */
    public function getHosts($perPage = 10)
    {
        $hosts = User::whereRole(2)->with('hostAppointments')->latest()->paginate($perPage);
        // $hosts = User::whereRole(2)->with(['hostAppointments' => function ($query) {
        //         $query->orderBy('created_at', 'desc');
        //     }])->paginate($perPage);
        return $hosts;
    }

    /**
     * Create new user.
     *
     * @param first_name,last_name,email,role
     * @return User
     **/
    public function save($data)
    {

        $data['password'] = bcrypt($data['password']);
        // create new user
        $user = User::create($data);

        // send mail to the user with account information
        //Mail::to($user->email)->send(new UserMail($user, $password));

        return $user;
    }

    /**
     * Delete User
     *
     * @param $id
     */
    public function delete($id)
    {
        $user = User::find($id);
        if ($user)
            $user->delete();
    } 
}
