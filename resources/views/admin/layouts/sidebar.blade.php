<aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <!-- @if(auth()->user()->role == 1) -->
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.hosts')}}" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Hosts</span></a></li>
                       <!--  @elseif(auth()->user()->role == 2)
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.host_appointments',['host_id' => auth()->user()->id])}}" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Appointments</span></a></li>
                        @endif -->
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>