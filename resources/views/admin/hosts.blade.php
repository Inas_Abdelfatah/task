@extends('admin.layouts.master')

@section('title')
    Hosts
@endsection
@section('content')
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Hosts</h5> -->
                    <h5 class="card-title">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add-new-host" class="btn m-t-20 btn-info waves-effect waves-light">
                        <i class="ti-plus"></i> Add New Host
                    </a></h5>
                    @include('admin.add_host')
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Email</th>
                                    <th>Appointments</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($hosts as $host)
                                    <tr>
                                        <td>{{$host->first_name}}</td>
                                        <td>{{$host->last_name}}</td>
                                        <td>{{$host->email}}</td>
                                        <td> <a href="{{route('admin.host_appointments',['host_id' => $host->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a> </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </tfoot> -->
                        </table>
                        {{ $hosts->links() }}
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        @if (count($errors) > 0)
            $('#add-new-host').modal('show');
        @endif
    </script>
@endsection