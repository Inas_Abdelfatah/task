<!-- Modal Add Category -->
                <div class="modal fade none-border" id="add-new-host">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form method="POST" action="{{route('admin.add_host')}}">
                                    @csrf
                                <div class="modal-header">
                                    <h4 class="modal-title"><strong>Add</strong> Host </h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">First Name</label>
                                                <input class="form-control form-white" placeholder="Enter first name" type="text" name="first_name" value="{{old('first_name')}}" />
                                                @if ($errors->has('first_name'))
                                                    <span style="color: red;">
                                                        {{ $errors->first('first_name') }}
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Last Name</label>
                                                <input class="form-control form-white" placeholder="Enter last name" type="text" name="last_name" value="{{old('last_name')}}"/>
                                                @if ($errors->has('last_name'))
                                                    <span style="color: red;"> {{ $errors->first('last_name') }} </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Email</label>
                                                <input class="form-control form-white" placeholder="Enter email" type="email" name="email" value="{{old('email')}}"/>
                                                @if ($errors->has('email'))
                                                    <span style="color: red;"> {{ $errors->first('email') }} </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Password</label>
                                                <input class="form-control form-white" placeholder="Enter password" type="text" name="password" />
                                                @if ($errors->has('password'))
                                                    <span style="color: red;"> {{ $errors->first('password') }} </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Password confirmation</label>
                                                <input class="form-control form-white" placeholder="Enter password" type="text" name="password_confirmation" />
                                                @if ($errors->has('password_confirmation'))
                                                    <span style="color: red;"> {{ $errors->first('password_confirmation') }} </span>
                                                @endif
                                            </div>
                                            <input type="hidden" name="role" value="2">

                                        </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->