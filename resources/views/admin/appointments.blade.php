@extends('admin.layouts.master')

@section('title')
    Appointments
@endsection
@section('content')
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class="card-title">Hosts</h5> -->
                    <h5 class="card-title">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add-new-appointment" class="btn m-t-20 btn-info waves-effect waves-light">
                        <i class="ti-plus"></i> Add New Appointment
                    </a></h5>
                    @include('admin.add_appointment')

                    <h5 class="card-title">Non-Reserved Appointments</h5>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>title</th>
                                    <th>date</th>
                                    <th>start time</th>
                                    <th>end time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($appointments['non_reserved_appointments'] as $appointment)
                                    <tr>
                                        <td id="appointment_id" data-appointment_id="{{$appointment->id}}">{{$appointment->title}}</td>
                                        <td>{{$appointment->date}}</td>
                                        <td>{{$appointment->time}}</td>
                                        <td>{{$appointment->end_time}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $appointments['non_reserved_appointments'] ->links() }}
                    </div>

                    <h5 class="card-title">Reserved Appointments</h5>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>title</th>
                                    <th>date</th>
                                    <th>start time</th>
                                    <th>end time</th>
                                    <th>Attendee</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($appointments['reserved_appointments'] as $appointment)
                                    <tr>
                                        <td>{{$appointment->title}}</td>
                                        <td>{{$appointment->date}}</td>
                                        <td>{{$appointment->time}}</td>
                                        <td>{{$appointment->end_time}}</td>
                                        <td>{{$appointment->attendee()->first()->first_name}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </tfoot> -->
                        </table>
                        {{ $appointments['reserved_appointments']->links() }}
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        @if (count($errors) > 0)
            $('#add-new-appointment').modal('show');
        @endif


    </script>
@endsection