<!-- Modal Add Category -->
                <div class="modal fade none-border" id="add-new-appointment">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form method="POST" action="{{route('admin.add_appointment')}}">
                                    @csrf
                                <div class="modal-header">
                                    <h4 class="modal-title"><strong>Add</strong> Appointment </h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Title</label>
                                                <input class="form-control form-white" placeholder="Enter title" type="text" name="title" value="{{old('title')}}" />
                                                @if ($errors->has('title'))
                                                    <span style="color: red;">
                                                        {{ $errors->first('title') }}
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Date</label>
                                                <input class="form-control form-white" placeholder="Enter last name" type="date" name="date" value="{{old('date')}}"/>
                                                @if ($errors->has('date'))
                                                    <span style="color: red;"> {{ $errors->first('date') }} </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Start Time</label>
                                                <input class="form-control form-white" placeholder="Enter last name" type="time" name="time" value="{{old('time')}}"/>
                                                @if ($errors->has('time'))
                                                    <span style="color: red;"> {{ $errors->first('time') }} </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">End Time</label>
                                                <input class="form-control form-white" placeholder="Enter last name" type="time" name="end_time" value="{{old('end_time')}}"/>
                                                @if ($errors->has('end_time'))
                                                    <span style="color: red;"> {{ $errors->first('end_time') }} </span>
                                                @endif
                                            </div>
                                            <input type="hidden" name="host_id" value="{{$host_id}}">

                                        </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->