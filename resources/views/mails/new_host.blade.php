@component('mail::message')

Dear [{{$user->first_name}} {{$user->last_name}}],

<br>

Account Information:
<ul>
    <li>Your Email is [{{$user->email}}]</li>
    @if($password != null)
        <li>your password is {{$password}}</li>
    @endif
</ul>
<br>

<br>
Thank you,
<br>
Inas Abdelfatah.
<br>

@endcomponent