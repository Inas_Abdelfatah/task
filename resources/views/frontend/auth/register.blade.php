@extends('frontend.auth.master')
@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-top border-secondary">
                <div>
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db">Register</span>
                    </div>
                    <!-- Form -->
                    <form class="form-horizontal m-t-20" action="{{route('frontend.postRegister')}}" method="POST">
                    	@csrf
                    	<input type="hidden" name="role" value="3">
                        <div class="row p-b-30">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control form-control-lg" placeholder="First Name" aria-label="Username" aria-describedby="basic-addon1" required>
                                    @if ($errors->has('first_name'))
                                        <span style="color: red;"> {{ $errors->first('first_name') }} </span>
                                    @endif
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control form-control-lg" placeholder="Last Name" aria-label="Username" aria-describedby="basic-addon1" required>
                                    @if ($errors->has('last_name'))
                                        <span style="color: red;"> {{ $errors->first('last_name') }} </span>
                                    @endif
                                </div>
                                <!-- email -->
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
                                    </div>
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1" required>
                                    @if ($errors->has('email'))
                                        <span style="color: red;"> {{ $errors->first('email') }} </span>
                                    @endif
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" name="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required>
                                    @if ($errors->has('password'))
                                        <span style="color: red;"> {{ $errors->first('password') }} </span>
                                    @endif
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-info text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" name="password_confirmation" class="form-control form-control-lg" placeholder=" Confirm Password" aria-label="Password" aria-describedby="basic-addon1" required>
                                    @if ($errors->has('password_confirmation'))
                                    <br/>
                                        <p style="color: red;"> {{ $errors->first('password_confirmation') }} </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <button class="btn btn-block btn-lg btn-info" type="submit">Sign Up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection