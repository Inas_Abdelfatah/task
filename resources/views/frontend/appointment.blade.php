@extends('frontend.layouts.master')

@section('styles')

@endsection

@section('content')

        <input type="hidden" name="display_name" id="display_name" value="{{auth()->user()->first_name}}">
        <input type="hidden" name="meeting_number" id="meeting_number" value="{{$appointment->meeting_id}}">
        <input type="hidden" name="meeting_pwd" id="meeting_pwd"  value="{{$appointment->password}}">
        <input type="hidden" name="leaveUrl" id="leaveUrl"  value="{{route('frontend.dashboard')}}">
        <!-- role: 1 for host; 0 for attendee or webinar -->
        @if(auth()->user()->role == 2)
            <input type="hidden" name="meeting_role" id="meeting_role"  value="1">
        @else
            <input type="hidden" name="meeting_role" id="meeting_role"  value="0">
        @endif


        <!-- added on import -->
        <!-- <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white">{{$appointment->title}}</h1>
                    <h6 class="text-white">{{ $appointment->date}} <time>{{$appointment->time}}</time></h6>
                    <a href="#" id="join_meeting" class="btn btn-warning"> Join Meeting </a>
                </div>
            </div>
        </div>
        <div id="zmmtg-root"></div>
        <div id="aria-notify-area"></div>
 -->
        <!-- added on meeting init -->
        <!-- <div class="ReactModalPortal"></div>
        <div class="ReactModalPortal"></div>
        <div class="ReactModalPortal"></div>
        <div class="ReactModalPortal"></div>
        <div class="global-pop-up-box"></div>
        <div class="sharer-controlbar-container sharer-controlbar-container--hidden"></div> -->
        
@endsection
@section('scripts')
    <!-- <script type="module" src="{{url('node_modules/@zoomus/websdk')}}"></script>
    <script src="{{ asset('assets/js/zoom/index.js') }}"></script> -->

    <!-- import ZoomMtg -->
    <script src="https://source.zoom.us/zoom-meeting-1.7.6.min.js"></script>
    <script src="{{ asset('assets/js/meeting.js') }}"></script>
    
@endsection