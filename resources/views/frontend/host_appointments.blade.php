@extends('frontend.layouts.master')

@section('title')
    Appointments
@endsection
@section('content')
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Non-Reserved Appointments</h5>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>title</th>
                                    <th>date</th>
                                    <th>start time</th>
                                    <th>end time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($appointments['non_reserved_appointments'] as $appointment)
                                    <tr>
                                        <td>{{$appointment->title}}</td>
                                        <td>{{$appointment->date}}</td>
                                        <td>{{$appointment->time}}</td>
                                        <td>{{$appointment->end_time}}</td>
                                        
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $appointments['non_reserved_appointments'] ->links() }}
                    </div>

                    <h5 class="card-title">Reserved Appointments</h5>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>title</th>
                                    <th>date</th>
                                    <th>start time</th>
                                    <th>end time</th>
                                    <th>Attendee</th>
                                    <th>init meeting</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($appointments['reserved_appointments'] as $appointment)
                                    <tr>
                                        <td id="appointment_id" data-appointment_id="{{$appointment->id}}">{{$appointment->title}}</td>
                                        <td>{{$appointment->date}}</td>
                                        <td>{{$appointment->time}}</td>
                                        <td>{{$appointment->end_time}}</td>
                                        <td>{{$appointment->attendee()->first()->first_name}}</td>
                                        <td id="init_meeting{{$appointment->id}}">
                                            @if($appointment->is_valid == 1)
                                                <a href="{{route('frontend.joinMeeting',['appointment_id' => $appointment->id])}}" class="btn btn-warning"> Init Meeting </a>
                                            @else
                                                <a href="#" class="btn btn-danger"> Time out </a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $appointments['reserved_appointments']->links() }}
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('scripts')
<!-- import ZoomMtg -->
    <!-- <script src="https://source.zoom.us/zoom-meeting-1.7.6.min.js"></script>
<script src="{{ asset('assets/js/init_meeting.js') }}"></script>
    <script type="text/javascript">
        
    </script> -->
@endsection