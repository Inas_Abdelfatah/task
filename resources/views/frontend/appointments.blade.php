@extends('frontend.layouts.master')

@section('title')
    Appointments
@endsection
@section('content')
    <div class="row">
        <!-- Column -->
        @forelse($appointments as $appointment)
        <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white">{{$appointment->title}}</h1>
                    <h6 class="text-white">{{ $appointment->date}}</h6>
                    <h6 class="text-white">from: <time>{{$appointment->time}} to: <time>{{$appointment->end_time}}</time></time></h6>
                    @if($appointment->is_valid == 1)
                        <a href="{{route('frontend.joinMeeting',['appointment_id' => $appointment->id])}}" class="btn btn-warning"> Join Meeting </a>
                    @else
                        <a href="#" class="btn btn-danger">time out</a>
                    @endif
                </div>
            </div>
        </div>
        @empty
        <p>there is no available appontments</p>
        @endforelse
        
    </div>
@endsection
