@extends('frontend.layouts.master')

@section('title')
    Appointments
@endsection
@section('content')
	<div class="row">
        <!-- Column -->
        @forelse($appointments as $appointment)
        <div class="col-md-6 col-lg-3">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white">{{$appointment->title}}</h1>
                    <h6 class="text-white">{{ $appointment->date }}</h6>
                    <h6 class="text-white">from: <time>{{$appointment->time}} to: <time>{{$appointment->end_time}}</time></time></h6>
                    <a href="{{route('frontend.reserve_appointment',['id' => $appointment->id])}}" class="btn btn-warning"> Reserve </a>
                </div>
            </div>
        </div>
        @empty
        <p>there is no available appontments</p>
        @endforelse
        
    </div>
@endsection